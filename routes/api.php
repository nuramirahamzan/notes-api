<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Get all notes
Route::get('/notes', [NoteController::class, 'index']);

// Get a note by ID
Route::get('/notes/{id}', [NoteController::class, 'show']);

// Create a new note
Route::post('/notes', [NoteController::class, 'store']);

// Update a note by ID
Route::put('/notes/{id}', [NoteController::class, 'update']);

// Delete a note by ID
Route::delete('/notes/{id}', [NoteController::class, 'destroy']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
