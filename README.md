# Notes Management API

This repository contains a simple Notes Management API built with PHP using the Laravel framework. The API allows you to perform basic CRUD (Create, Read, Update, Delete) operations on notes. Each note has an ID, title, and content.

## Requirements

- PHP (>= 7.3)
- [Composer](https://getcomposer.org/)
- MySQL or PostgreSQL
- Laravel (>= 8.0)

## Setup

1. **Clone the repository:**

    ```bash
    git clone https://github.com/your-username/notes-api.git
    ```

2. **Navigate to the project directory:**

    ```bash
    cd notes-api
    ```

3. **Install dependencies:**

    ```bash
    composer install
    ```

4. **Create a copy of the `.env.example` file and name it `.env`. Update the database configuration with your credentials:**

    ```bash
    cp .env.example .env
    ```

5. **Generate the application key:**

    ```bash
    php artisan key:generate
    ```

6. **Run database migrations:**

    ```bash
    php artisan migrate
    ```

7. **Start the development server:**

    ```bash
    php artisan serve
    ```

    The API will be accessible at [http://127.0.0.1:8000](http://127.0.0.1:8000).

## API Endpoints

### 1. GET /notes

Retrieve a list of all notes.

#### Example Request:

http://127.0.0.1:8000/api/notes

[
    {
        "id": 4,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:01.000000Z",
        "updated_at": "2023-11-29T18:46:01.000000Z"
    },
    {
        "id": 5,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:02.000000Z",
        "updated_at": "2023-11-29T18:46:02.000000Z"
    },
    {
        "id": 6,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:03.000000Z",
        "updated_at": "2023-11-29T18:46:03.000000Z"
    },
    {
        "id": 7,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:03.000000Z",
        "updated_at": "2023-11-29T18:46:03.000000Z"
    },
    {
        "id": 8,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:04.000000Z",
        "updated_at": "2023-11-29T18:46:04.000000Z"
    },
    {
        "id": 9,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:04.000000Z",
        "updated_at": "2023-11-29T18:46:04.000000Z"
    },
    {
        "id": 10,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:04.000000Z",
        "updated_at": "2023-11-29T18:46:04.000000Z"
    }
]


### 2. POST /notes

http://127.0.0.1:8000/api/notes

[
    {
        "message": "Note created successfully",
        "data": {
            "title": "123 What is Lorem Ipsum?",
            "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "updated_at": "2023-11-29T18:51:59.000000Z",
            "created_at": "2023-11-29T18:51:59.000000Z",
            "id": 11
        }
    }

]

### 3. GET /notes/{id}

http://127.0.0.1:8000/api/notes/10

[
    {
        "id": 10,
        "title": "What is Lorem Ipsum?",
        "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2023-11-29T18:46:04.000000Z",
        "updated_at": "2023-11-29T18:46:04.000000Z"
    }
]

### 4. PUT /notes/{id}

http://127.0.0.1:8000/api/notes/10

[
    {
        "message": "Note updated successfully",
        "data": {
            "id": 10,
            "title": "My Profile123",
            "content": "My name is Nuramirah123",
            "created_at": "2023-11-29T18:46:04.000000Z",
            "updated_at": "2023-11-30T01:28:11.000000Z"
        }
    }
]

### 5. DELETE /notes/{id}

http://127.0.0.1:8000/api/notes/10

[
    {
        "message": "Note deleted successfully"
    }
]
